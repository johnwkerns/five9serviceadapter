﻿namespace Common
{
  public class Settings
  {
    public string AdminUrl { get; set; }
    public string SupervisorUrl { get; set; }
    public string Login { get; set; }
    public string Pwd { get; set; }
    public int SupervisorTimeZone { get; set; }
    public int SupervisorTimeout { get; set; }
  }
}