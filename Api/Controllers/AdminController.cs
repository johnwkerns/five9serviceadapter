﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Connector;
using System.Threading.Tasks;
using System.Net.Http;
using Microsoft.Extensions.Options;
using Common;
using Connector.Five9AdminService;

namespace Api.Controllers
{
  [Route("api/[controller]")]
  public class AdminController : Controller
  {
    private readonly IOptions<Settings> _settings;

    public AdminController(IOptions<Settings> settings)
    {
      _settings = settings;
    }


    // GET api/values
    [HttpGet]
    public async Task<IActionResult> Get()
    {
      var adminConn = new Admin(_settings.Value);
      //var result = await adminConn.GetListsWithSkill(".*", "AETNA");

      return Ok();
    }

    [HttpGet]
    [Route("GetListsInfo/{listName}")]
    public async Task<IActionResult> GetListsInfo(string listName)
    {
      var adminConn = new Admin(_settings.Value);
      var result = await adminConn.GetListsInfo(listName);

      return Ok(result);
    }








    //// GET api/values/5
    //[HttpGet("{id}")]
    //public string Get(int id)
    //{
    //  return "value";
    //}

    //// POST api/values
    //[HttpPost]
    //public void Post([FromBody]string value)
    //{
    //}

    //// PUT api/values/5
    //[HttpPut("{id}")]
    //public void Put(int id, [FromBody]string value)
    //{
    //}

    //// DELETE api/values/5
    ///// <summary>
    ///// Delete API Value
    ///// </summary>
    ///// <remarks>This API will delete the values.</remarks>
    ///// <param name="id"></param>
    //[HttpDelete("{id}")]
    //public void Delete(int id)
    //{
    //}
  }
}