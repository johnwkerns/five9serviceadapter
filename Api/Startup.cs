﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.Swagger.Model;
using Serilog;
using Common;

namespace Api
{
  public class Startup
  {
    public Startup(IHostingEnvironment env)
    {
      var builder = new ConfigurationBuilder()
          .SetBasePath(env.ContentRootPath)
          .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
          .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
          .AddEnvironmentVariables();
      Configuration = builder.Build();
    }

    public IConfigurationRoot Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      // Add framework services.
      var xmlPath = GetXmlCommentsPath();
      services.AddMvc();
      services.AddSwaggerGen();
      services.ConfigureSwaggerGen(options =>
      {
        options.SingleApiVersion(new Info
        {
          Version = "v1",
          Title = "Five9ServiceAdapter API",
          Description = "Calling Five9 Services",
          Contact = new Contact() { Name = "John Kerns, Elite Programmer and Smooth Operator", Email = "john.kerns@AdvanceHealth.com", Url = "" }
        });
        options.IncludeXmlComments(xmlPath);
      });


      services.Configure<Settings>(Configuration.GetSection("Settings"));
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
    {
      loggerFactory.AddConsole(Configuration.GetSection("Logging"));
      loggerFactory.AddDebug();
      loggerFactory.AddSerilog(dispose: true);

      app.UseMvc();

      app.UseSwagger();
      app.UseSwaggerUi();
    }

    private string GetXmlCommentsPath()
    {
      var app = PlatformServices.Default.Application;
      return System.IO.Path.Combine(app.ApplicationBasePath, "Api.xml");
    }
  }
}