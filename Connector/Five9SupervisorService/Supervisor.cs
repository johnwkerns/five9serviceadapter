﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

using Connector.com.five9.supervisor.api;
using System.Net;

namespace Connector.Five9SupervisorService
{
  public class Supervisor
  {
    private Settings _settings;
    public Supervisor(Settings settings)
    {
      _settings = settings;
    }

    private WsSupervisorService InitSupervisorService()
    {
      WsSupervisorService supervisorService = new WsSupervisorService();
      //System.Net.CredentialCache serviceCredentials = new System.Net.CredentialCache();
      NetworkCredential netCredentials = new NetworkCredential(_settings.Login, _settings.Pwd);

      //serviceCredentials.Add(new Uri(urlSupervisor), "Basic", netCredentials);
      supervisorService.Credentials = netCredentials.GetCredential(new Uri(_settings.SupervisorUrl), "Basic");
      supervisorService.PreAuthenticate = true;

      //set session
      var theParamsSession = new setSessionParameters();
      var theParamsGetStat = new getStatistics();
      var theViewSetting = new viewSettings();
      theViewSetting.timeZone = _settings.SupervisorTimeZone;
      theViewSetting.statisticsRange = statisticsRange.CurrentDay;
      theViewSetting.statisticsRangeSpecified = true;
      theViewSetting.rollingPeriod = rollingPeriod.Today;
      theViewSetting.rollingPeriodSpecified = true;
      theParamsSession.viewSettings = theViewSetting;

      var theResponseSession = supervisorService.setSessionParameters(theParamsSession);
      supervisorService.Timeout = _settings.SupervisorTimeout;

      return supervisorService;
    }

  }
}
