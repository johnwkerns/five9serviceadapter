﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Common;

using Connector.com.five9.admin.api;


namespace Connector.Five9AdminService
{
  public class Admin
  {
    private Settings _settings;
    public Admin(Settings settings)
    {
      _settings = settings;
    }


    //private readonly string five9SupervisorLogin = "five9.test@advancehlth.com";
    //private readonly string five9SupervisorPwd = "advance55";

    ////PROD
    ////private readonly string five9SupervisorLogin = "john.kerns@advancehealth.com";
    ////private readonly string five9SupervisorPwd = "advance123";

    //private readonly string urlSupervisor = "https://api.five9.com/wsadmin/v2/SupervisorWebService";
    //private readonly string urlAdmin = "https://api.five9.com/wsadmin/v2/AdminWebService";

    //public async Task<List<string>> GetListsWithSkill(string list, string skill)
    //{
    //  var adminService = await InitAdminService();
    //  getListsInfo listInfo = new getListsInfo { listNamePattern = list };
    //  getSkillsInfo skillsInfo = new getSkillsInfo { skillNamePattern = skill };

    //  var result = new List<string>();

    //  try
    //  {
    //     result = (from l in adminService.getListsInfo(listInfo).Select(x => x.name).ToList()
    //                  join s in adminService.getSkillsInfo(skillsInfo).Select(x => x.users.Select(u => u.userName).ToList()).SingleOrDefault() on l equals s
    //                  select l
    //          ).ToList();
    //  }
    //  catch (Exception e)
    //  {

    //    throw;
    //  }

    //  return result;
    //}

    private async Task<WsAdminService> InitAdminService()
    {
      WsAdminService adminService = new WsAdminService();
      System.Net.CredentialCache serviceCredentials = new System.Net.CredentialCache();
      NetworkCredential netCredentials = new NetworkCredential(_settings.Login, _settings.Pwd);

      serviceCredentials.Add(new Uri(_settings.AdminUrl), "Basic", netCredentials);
      adminService.Credentials = netCredentials.GetCredential(new Uri(_settings.AdminUrl), "Basic");
      adminService.PreAuthenticate = true;

      return adminService;
    }

    public async Task<int> GetListsInfo(string list)
    {
      var adminService = await InitAdminService();
      getListsInfo listInfo = new getListsInfo { listNamePattern = list };

      // var result =  adminService.getListsInfo(listInfo).Select(x => x.name).ToList();

      var test = adminService.getListsInfo(listInfo).ToList();

      var result = adminService.getListsInfo(listInfo)
                   .Select(x => x.size).SingleOrDefault();

      return result;
    }
  }
}